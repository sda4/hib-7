package sda.jpa;

import sda.jpa.model.*;

import javax.persistence.*;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

public class Example1 {

    // TODO:
    // 1. uruchomic program dwa razy jednoczesnie, kontynuowac po kolei pierwszy i drugi proces
    // 2. zmienic program, aby tylko jedna z transakcji sie powiodla
    // 3. zmienic program, zeby jedna z transakcji czekala az druga sie skonczy

    public static void main(String[] args) {
        EntityManagerFactory emf = null;
        try {
            emf = Persistence.createEntityManagerFactory("sda.jpa.1");
            EntityManager em = emf.createEntityManager();

            em.getTransaction().begin();

            Ksiazka ks = em.find(Ksiazka.class, 1L);

            Random r = new Random();
            int liczba = r.nextInt();

            ks.setTytul("Nowy tytul" + liczba);

            System.out.println("Wylosowana liczba: " + liczba);
            System.out.println("Wcisnij enter aby kontynuowac");
            Scanner sc = new Scanner(System.in);
            sc.hasNextLine();

            em.getTransaction().commit();
            em.close();
        } finally {
            if (emf != null) {
                emf.close();
            }
        }
    }
}
