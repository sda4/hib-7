package sda.jpa.model;

import lombok.Data;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Table;

@Data
@Entity
@Table(name="klient_emeryt")
public class KlientEmeryt extends Klient {

    private Integer znizka;
}
