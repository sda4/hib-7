package sda.jpa.model;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Entity
@Data
@Table(name="klient")
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
public class Klient extends Czlowiek {

    @Id
    @Column(name = "klient_id")
//    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long klientId;

    @OneToMany(mappedBy = "idOceny.oceniajacy")
    private List<OcenaKsiazki> oceny;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_polecony_przez")
    private Klient polecajacy;

    @OneToMany(mappedBy = "klient")
    private List<Recenzja> recenzje;

    @Transient
    private Long licznikKsiazek;
}
