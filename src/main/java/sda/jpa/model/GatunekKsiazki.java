package sda.jpa.model;

import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "gatunek_ksiazki")
@Data
public class GatunekKsiazki {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "gatunek_id")
    private Long idGatunku;

    @Column(name="nazwa")
    private String nazwaGatunku;

    @ManyToMany(mappedBy = "gatunki")
    private List<Ksiazka> ksiazkiGatunku;
}
