package sda.jpa.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AutorKsiazki {

    @Column(name = "autor_imie")
    private String imieAutora;

    @Column(name = "autor_nazwisko")
    private String nazwiskoAutora;
}
